import sys
from .session import CustomGUISession


def main(argv):
    (
        session_name,
        host,
        port,
        cluster_name,
        db,
        password,
        debug,
        remaining_args,
    ) = CustomGUISession.parse_command_line_args(argv)

    session = CustomGUISession(session_name=session_name, debug=debug)
    session.cmds.Cluster.connect(host, port, cluster_name, db, password)

    session.start()
    session.close()

if __name__ == '__main__':
    main(sys.argv[1:])