import os, sys, subprocess

def valid_attr_name(name):
    '''
    Copied from kabaret.flow.Map
    '''
    try:
        exec(name + '=None') in {}
    except:
        return False
    return True

def open_file(filename):
    '''From https://stackoverflow.com/a/17317468'''

    if sys.platform == 'win32':
        os.startfile(filename)
    else:
        opener = 'open' if sys.platform == 'darwin' else 'xdg-open'
        subprocess.call([opener, filename])
